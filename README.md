# README #

### MiSS Project - SymbolicMed ###

Symbolic regression on AgE platform.

### Instruction ###
1. Clone repo
1. Import to IDE (optional)
1. Define file with data and parameters in spring-config.xml
1. Run gradle med
1. Results will be available in logs.log

### Status ###
Report for this project is available [here](https://nequeq.bitbucket.io/projects/missmed/miss-symbolicmed.pdf) (in Polish but contains plots with examples).
Probably project will be improved in future.

### License ###
Apache License 2.0. Feel free to use it and share with results.

### Developers ###

* Wojciech Pater
* Paweł Kocot