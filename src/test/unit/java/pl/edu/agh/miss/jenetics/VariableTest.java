package pl.edu.agh.miss.jenetics;

import org.junit.Before;
import org.junit.Test;
import pl.edu.agh.miss.jenetics.beans.equation.*;

import java.math.BigDecimal;
import java.util.HashMap;

import static org.junit.Assert.assertEquals;

/**
 * Test basic operations in jenetics lib
 */
public class VariableTest {

    private HashMap<String, Double> arguments;

    @Before
    public void initPerTestCase() {
        arguments = new HashMap<>();
        arguments.put("x", 69d);
    }

    @Test
    // Evaluate variable
    public void evaluateVarWithCorrectArguments() {
        Variable var = new Variable("x");
        Double expectedValue = 69d;
        Double calculatedValue = var.eval(arguments);
        assertEquals(expectedValue, calculatedValue);
    }

    @Test
    // Evaluate variable - negative scenario
    // By default eval() functions returns 0 if value for variable is not found in map
    public void evaluateVarWithIncorrectArguments() {
        Variable var = new Variable("y");
        Double expectedValue = 0d;
        Double calculatedValue = var.eval(arguments);
        assertEquals(expectedValue, calculatedValue);
    }
}
