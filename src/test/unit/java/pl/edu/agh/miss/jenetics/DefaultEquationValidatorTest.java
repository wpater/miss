package pl.edu.agh.miss.jenetics;

import org.junit.Test;
import pl.edu.agh.miss.jenetics.beans.EquationPart;
import pl.edu.agh.miss.jenetics.beans.equation.*;
import pl.edu.agh.miss.jenetics.core.validators.DefaultEquationValidator;

import java.math.BigDecimal;

import static org.junit.Assert.assertTrue;

public class DefaultEquationValidatorTest {

    @Test
    // Validation for simple tree 3*x + 2*y
    public void validateTree() {
        EquationPart two = new Constant(2d);
        EquationPart three = new Constant(3d);
        EquationPart x = new Variable("x");
        EquationPart y = new Variable("y");
        EquationPart leftMulti = new Multiply(three, x);
        EquationPart rightMulti = new Multiply(two, y);
        BiEquationPart root = new Sum(leftMulti, rightMulti);

        DefaultEquationValidator validator = new DefaultEquationValidator();
        boolean result = validator.isValid(root);
        assertTrue(result);
    }
}
