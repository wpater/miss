package pl.edu.agh.miss.jenetics;

import org.junit.Before;
import org.junit.Test;
import pl.edu.agh.miss.age.functions.fitness.MedFitness;
import pl.edu.agh.miss.age.functions.fitness.MedFitnessEvaluator;
import pl.edu.agh.miss.age.functions.mutation.BasicMutation;
import pl.edu.agh.miss.age.functions.mutation.MedMutation;
import pl.edu.agh.miss.jenetics.beans.EquationPart;
import pl.edu.agh.miss.jenetics.beans.equation.*;

import java.util.HashMap;

import static org.junit.Assert.assertFalse;

public class BasicMutationTest {

    private BiEquationPart root;
    private HashMap<String, Double> arguments;
    private MedMutation mutation;
    private MedFitness fitness;

    @Before
    public void initPerTestCase() {
        fitness = new MedFitnessEvaluator("dataFile.txt");
        // Create simple tree (2*x + y) + (2*y + x)
        EquationPart two = new Constant(2d);
        EquationPart x = new Variable("x");
        EquationPart y = new Variable("y");
        BiEquationPart twoX = new Multiply(two, x);
        BiEquationPart twoY = new Multiply(two, y);
        BiEquationPart firstSum = new Sum(twoX, y);
        BiEquationPart secondSum = new Sum(twoY, x);
        root = new Sum(firstSum, secondSum);
        System.out.println(root);
        // Add values for variables in tree
        arguments = new HashMap<>();
        arguments.put("x", 2d);
        arguments.put("y", 3d);
        // Create mutation
        mutation = new BasicMutation(1, 1, fitness);
    }

    @Test
    // Check that tree after mutation is possible to evaluate and is different than old tree
    public void validateTreeAfterMutation() {
        EquationPart newRoot = mutation.mutateTree(root);
        System.out.println(newRoot);
        System.out.println(newRoot.eval(arguments));
        assertFalse(root.toString().equals(newRoot.toString()));
    }
}
