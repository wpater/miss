package pl.edu.agh.miss.jenetics;

import org.junit.Before;
import org.junit.Test;
import pl.edu.agh.miss.age.functions.fitness.MedFitness;
import pl.edu.agh.miss.age.functions.fitness.MedFitnessEvaluator;
import pl.edu.agh.miss.age.functions.mutation.BasicMutation;
import pl.edu.agh.miss.age.functions.mutation.MedMutation;
import pl.edu.agh.miss.jenetics.beans.EquationPart;
import pl.edu.agh.miss.jenetics.beans.equation.Sinus;
import pl.edu.agh.miss.jenetics.beans.equation.Variable;

/**
 * This test is created for easier calculate fitness for found functions.
 * Define in initPerTestCase your equation and file with data and just run test, fitness will be printed.
 */
public class FitnessTest {

    private EquationPart root;
    private MedMutation mutation;
    private MedFitness fitness;

    @Before
    public void initPerTestCase() {
        fitness = new MedFitnessEvaluator("dataFile_sinx.txt");
        mutation = new BasicMutation(1, 1, fitness);
        root = new Sinus(new Variable("In1"));
        //root = new Multiply(new Variable("In1"), new Sinus(new Variable("In1")));
    }

    @Test
    // Calculate fitness for given function
    public void calculateFitness() {
        System.out.println(root);
        System.out.println("Fitness for tree is: " + fitness.fitness(root));
    }
}
