package pl.edu.agh.miss.jenetics;

import org.junit.Before;
import org.junit.Test;
import pl.edu.agh.miss.jenetics.beans.EquationPart;
import pl.edu.agh.miss.jenetics.beans.equation.*;

import java.util.HashMap;

import static org.junit.Assert.assertEquals;

/**
 * Test basic operations in jenetics lib
 */
public class SimpleTreeTest {

    private BiEquationPart root;
    private EquationPart multi;
    private EquationPart y;
    private HashMap<String, Double> arguments;

    @Before
    public void initPerTestCase() {
        // Create simple tree 2*x + y
        EquationPart two = new Constant(2d);
        EquationPart x = new Variable("x");
        y = new Variable("y");
        multi = new Multiply(two, x);
        root = new Sum(multi, y);
        // Add values for variables in tree
        arguments = new HashMap<>();
        arguments.put("x", 2d);
        arguments.put("y", 3d);
    }

    @Test
    // Evaluate tree (unit test?)
    public void evaluateTreeWithArguments() {
        Double expectedValue = 7d;
        Double calculatedValue = root.eval(arguments);
        assertEquals(expectedValue, calculatedValue);
    }

    @Test
    // Change tree to 2*x + 2*x
    public void evaluateTreeWithRight() {
        BiEquationPart newRoot = root.withRight(multi, Sum.class);
        assertEquals("((2.0*x)+(2.0*x))", newRoot.toString());
        Double firstExpectedValue = 8d;
        Double calculatedValue = newRoot.eval(arguments);
        assertEquals(firstExpectedValue, calculatedValue);
    }

    @Test
    // Change tree to y + y
    public void evaluateTreeWithLeft() {
        BiEquationPart newRoot = root.withLeft(y, Sum.class);
        assertEquals("(y+y)", newRoot.toString());
        Double secondExpectedValue = 6d;
        Double calculatedValue = newRoot.eval(arguments);
        assertEquals(secondExpectedValue, calculatedValue);
    }

    @Test
    // Change tree to y + 2*x
    public void evaluateTreeAfterSwapNodes() {
        BiEquationPart newRoot = root.withLeft(y, Sum.class);
        newRoot = newRoot.withRight(multi, Sum.class);
        assertEquals("(y+(2.0*x))", newRoot.toString());
        Double secondExpectedValue = 7d;
        Double calculatedValue = newRoot.eval(arguments);
        assertEquals(secondExpectedValue, calculatedValue);
    }
}
