package pl.edu.agh.miss.jenetics.beans.equation;

import pl.edu.agh.miss.jenetics.beans.EquationPart;

import java.util.Map;

public class Subtract extends BiEquationPart {

    public Subtract(final EquationPart left, final EquationPart right) {
        super(left, right, Operations.SUBSTRACT.getOperator());
    }

    @Override
    public Double eval(final Map<String, Double> values) {
        return left.eval(values) - right.eval(values);
    }
}
