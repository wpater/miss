package pl.edu.agh.miss.jenetics.beans.equation;

import pl.edu.agh.miss.jenetics.beans.EquationPart;

import java.util.Map;

public class Sinus extends SingleEquationPart {
    public Sinus(EquationPart equationPart) {
        super(equationPart, Operations.SINUS.getOperator());
    }

    @Override
    public Double eval(Map<String, Double> values) {
        double val = getEquationPart().eval(values).doubleValue();
        return Math.sin(getEquationPart().eval(values));
    }
}
