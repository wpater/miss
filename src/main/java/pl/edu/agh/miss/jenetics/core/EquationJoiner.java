package pl.edu.agh.miss.jenetics.core;

import pl.edu.agh.miss.jenetics.beans.EquationPart;

import java.util.List;

public interface EquationJoiner {
    EquationPart join(final EquationPart equation,
                      final List<EquationPart> originalParts,
                      final List<EquationPart> newParts);
}