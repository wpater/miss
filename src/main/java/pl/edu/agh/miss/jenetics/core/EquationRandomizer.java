package pl.edu.agh.miss.jenetics.core;

import pl.edu.agh.miss.jenetics.beans.EquationPart;

import java.util.List;

public interface EquationRandomizer {
    EquationPart randomNodeFromTree(int origin, int bound, EquationPart root);
    List<EquationPart> getPathToNode(EquationPart node);
    EquationPart joinTrees(List<EquationPart> path, EquationPart nodeToChange, EquationPart newPart);
}
