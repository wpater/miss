package pl.edu.agh.miss.jenetics.beans.equation;

import java.math.BigDecimal;
import java.util.Map;

import static java.util.Objects.requireNonNull;

public class Constant extends IdentifiableEquationPart {
    private final Double value;

    public Constant(final Double value) {
        this.value = requireNonNull(value);
    }

    @Override
    public Double eval(final Map<String, Double> values) {
        return value;
    }

    @Override
    public boolean isTerminal() {
        return true;
    }

    @Override
    public String toString() {
        return value.toString();
    }
}