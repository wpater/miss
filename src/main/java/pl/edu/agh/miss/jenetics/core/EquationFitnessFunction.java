package pl.edu.agh.miss.jenetics.core;

import org.jenetics.Genotype;
import pl.edu.agh.miss.jenetics.beans.EquationGene;

public interface EquationFitnessFunction {
    Integer eval(final Genotype<EquationGene> gene);
}