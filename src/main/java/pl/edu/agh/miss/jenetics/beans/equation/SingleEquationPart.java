package pl.edu.agh.miss.jenetics.beans.equation;

import pl.edu.agh.miss.jenetics.beans.EquationPart;

import java.io.Serializable;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.Objects.requireNonNull;

public abstract class SingleEquationPart extends IdentifiableEquationPart implements Serializable {

    private EquationPart equationPart;
    private String operator;

    SingleEquationPart(EquationPart equationPart, String operator) {
        this.operator = requireNonNull(operator);
        this.equationPart = requireNonNull(equationPart);
    }

    public EquationPart getEquationPart(){
        return equationPart;
    }

    public String getOperator(){
        return operator;
    }

    public void setChild(EquationPart child) {
        this.equationPart = child;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }


    public <T extends SingleEquationPart> T withChild(final EquationPart equationPart, final Class<T> clazz) {
        try {
            return clazz.getConstructor(EquationPart.class).newInstance(equationPart);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public String toString() {
        return Stream.of(operator, "(", equationPart.toString(), ")").collect(Collectors.joining());
    }

    @Override
    public boolean isTerminal() {
        return false;
    }
}
