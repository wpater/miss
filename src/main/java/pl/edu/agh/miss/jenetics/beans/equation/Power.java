package pl.edu.agh.miss.jenetics.beans.equation;

import pl.edu.agh.miss.jenetics.beans.EquationPart;

import java.util.Map;

public class Power extends BiEquationPart {

    public Power(EquationPart left, EquationPart right) {
        super(left, right, Operations.POWER.getOperator());
    }

    @Override
    public Double eval(Map<String, Double> values) {
        return Math.pow(Math.abs(left.eval(values)), right.eval(values));
    }
}
