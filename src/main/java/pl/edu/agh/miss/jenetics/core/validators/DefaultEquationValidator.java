package pl.edu.agh.miss.jenetics.core.validators;

import pl.edu.agh.miss.jenetics.beans.EquationPart;
import pl.edu.agh.miss.jenetics.beans.equation.BiEquationPart;
import pl.edu.agh.miss.jenetics.beans.equation.SingleEquationPart;
import pl.edu.agh.miss.jenetics.beans.equation.Variable;
import pl.edu.agh.miss.jenetics.core.EquationValidator;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.Objects.requireNonNull;

//TODO: Remove or fix (div by 0)
public class DefaultEquationValidator implements EquationValidator {
    @Override
    public boolean isValid(final EquationPart equationPart) {
        requireNonNull(equationPart);
        Set<String> variables = getVariablesNames(equationPart);
        try {
            equationPart.eval(variables.stream().collect(Collectors.toMap(
                    Function.identity(),
                    e -> new Double(1)
            )));
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private Set<String> getVariablesNames(EquationPart equationPart) {
        return getAllVariables(equationPart).stream()
                .map(Variable::toString)
                .collect(Collectors.toSet());
    }

    private List<Variable> getAllVariables(final EquationPart equationPart) {
        final ArrayList<Variable> variables = new ArrayList<>();
        if (equationPart.isTerminal()) {
            if (equationPart instanceof Variable) {
                variables.add((Variable) equationPart);
            }
        } else if (equationPart instanceof BiEquationPart){
            variables.addAll(getAllVariables(((BiEquationPart) equationPart).getLeft()));
            variables.addAll(getAllVariables(((BiEquationPart) equationPart).getRight()));
        } else {
            variables.addAll(getAllVariables(((SingleEquationPart) equationPart).getEquationPart()));
        }
        return variables;
    }
}
