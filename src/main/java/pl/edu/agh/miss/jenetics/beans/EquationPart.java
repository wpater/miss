package pl.edu.agh.miss.jenetics.beans;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Map;

public interface EquationPart extends Serializable {
    Double eval(Map<String, Double> values);

    default Double eval() {
        return eval(null);
    }

    boolean isTerminal();
}
