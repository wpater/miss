package pl.edu.agh.miss.jenetics.beans.equation;

import pl.edu.agh.miss.jenetics.beans.EquationPart;

import java.util.Map;

public class Cosinus extends SingleEquationPart {
    public Cosinus(EquationPart equationPart) {
        super(equationPart, Operations.COSINUS.getOperator());
    }

    @Override
    public Double eval(Map<String, Double> values) {
        return Math.cos(getEquationPart().eval(values));
    }
}
