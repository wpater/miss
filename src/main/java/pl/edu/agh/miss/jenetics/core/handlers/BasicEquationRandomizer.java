package pl.edu.agh.miss.jenetics.core.handlers;

import pl.edu.agh.miss.jenetics.beans.EquationPart;
import pl.edu.agh.miss.jenetics.beans.equation.BiEquationPart;
import pl.edu.agh.miss.jenetics.beans.equation.SingleEquationPart;
import pl.edu.agh.miss.jenetics.core.EquationRandomizer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class BasicEquationRandomizer implements EquationRandomizer {

    private List<EquationPart> path;

    @Override
    public EquationPart randomNodeFromTree(int origin, int bound, EquationPart root) {
        EquationPart tempRoot = root;
        path = new ArrayList<>();
        final ThreadLocalRandom randomGenerator = ThreadLocalRandom.current();
        int depth = randomGenerator.nextInt(origin, bound);
        for (int i = 0; i < depth; i++) {
            if (tempRoot instanceof SingleEquationPart && ((SingleEquationPart) tempRoot).getEquationPart() != null) {
                path.add(tempRoot);
                tempRoot = ((SingleEquationPart) tempRoot).getEquationPart();
            } else if (tempRoot instanceof BiEquationPart) {
                if (((BiEquationPart) tempRoot).getLeft().isTerminal() && ((BiEquationPart) tempRoot).getRight().isTerminal()) break;
                if (randomGenerator.nextBoolean() && !((BiEquationPart) tempRoot).getRight().isTerminal()) {
                    path.add(tempRoot);
                    tempRoot = ((BiEquationPart) tempRoot).getRight();
                } else if (!((BiEquationPart) tempRoot).getLeft().isTerminal()) {
                    path.add(tempRoot);
                    tempRoot = ((BiEquationPart) tempRoot).getLeft();
                }
            }
        }
        Collections.reverse(path);
        return tempRoot;
    }

    //TODO: implement it, for now user has to call random and then getPath
    @Override
    public List<EquationPart> getPathToNode(EquationPart node) {
        return null;
    }

    @Override
    public EquationPart joinTrees(List<EquationPart> path, EquationPart nodeToChange, EquationPart newPart) {
        if (path.size() == 0)
            return newPart;
        EquationPart parent = path.get(0);
        if (parent instanceof BiEquationPart) {
            if (((BiEquationPart) parent).getLeft().equals(nodeToChange)) parent =
                    ((BiEquationPart) parent).withLeft(newPart, ((BiEquationPart) parent).getClass());
            else parent = ((BiEquationPart) parent).withRight(newPart, ((BiEquationPart) parent).getClass());
        }
        if (parent instanceof SingleEquationPart) {
            if (((SingleEquationPart) parent).getEquationPart().equals(nodeToChange)) parent =
                    ((SingleEquationPart) parent).withChild(newPart, ((SingleEquationPart) parent).getClass());
        }

        for (int i = 1; i < path.size(); i++) {
            if (path.get(i) instanceof BiEquationPart) {
                if (((BiEquationPart) path.get(i)).getLeft().equals(path.get(i - 1)))
                    parent = ((BiEquationPart) path.get(i))
                            .withLeft(parent, ((BiEquationPart) path.get(i)).getClass());
                else parent = ((BiEquationPart) path.get(i))
                        .withRight(parent, ((BiEquationPart) path.get(i)).getClass());
            } else if (path.get(i) instanceof SingleEquationPart) {
                if (((SingleEquationPart) path.get(i)).getEquationPart().equals(path.get(i - 1)))
                    parent = ((SingleEquationPart) path.get(i))
                            .withChild(parent, ((SingleEquationPart) path.get(i)).getClass());
            }
        }
        return parent;
    }

    public List<EquationPart> getPath() {
        return path;
    }
}
