package pl.edu.agh.miss.jenetics.beans.equation;

import pl.edu.agh.miss.jenetics.beans.EquationPart;

import java.util.Map;

public class Divide extends BiEquationPart {
    public Divide(final EquationPart left, final EquationPart right) {
        super(left, right, Operations.DIVIDE.getOperator());
    }

    @Override
    public Double eval(final Map<String, Double> values) {
        if (right.eval(values) == 0) return 1.0;
        return left.eval(values) / right.eval(values);
    }
}
