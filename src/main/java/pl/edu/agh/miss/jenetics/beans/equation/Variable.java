package pl.edu.agh.miss.jenetics.beans.equation;

import java.util.Map;

import static java.util.Objects.requireNonNull;

public class Variable extends IdentifiableEquationPart {
    private final String name;

    public Variable(final String name) {
        this.name = requireNonNull(name);
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public Double eval(final Map<String, Double> values) {
        return values.getOrDefault(name, 0d);
    }

    @Override
    public boolean isTerminal() {
        return true;
    }
}