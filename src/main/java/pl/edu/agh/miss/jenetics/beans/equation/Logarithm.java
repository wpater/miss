package pl.edu.agh.miss.jenetics.beans.equation;

import pl.edu.agh.miss.jenetics.beans.EquationPart;

import java.util.Map;

public class Logarithm extends SingleEquationPart {
    public Logarithm(EquationPart equationPart) {
        super(equationPart, Operations.LOGATIHM.getOperator());
    }

    @Override
    public Double eval(Map<String, Double> values) {
        if (getEquationPart().eval(values) == 0) return 1.0;
        return Math.log(Math.abs(getEquationPart().eval(values)));
    }
}
