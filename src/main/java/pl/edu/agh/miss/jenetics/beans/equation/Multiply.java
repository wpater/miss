package pl.edu.agh.miss.jenetics.beans.equation;

import pl.edu.agh.miss.jenetics.beans.EquationPart;

import java.util.Map;

public class Multiply extends BiEquationPart {

    public Multiply(final EquationPart left, final EquationPart right) {
        super(left, right, Operations.MULTIPLY.getOperator());
    }

    @Override
    public Double eval(final Map<String, Double> values) {
        return left.eval(values) * right.eval(values);
    }
}
