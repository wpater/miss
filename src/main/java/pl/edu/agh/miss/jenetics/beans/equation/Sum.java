package pl.edu.agh.miss.jenetics.beans.equation;

import pl.edu.agh.miss.jenetics.beans.EquationPart;

import java.math.BigDecimal;
import java.util.Map;

public class Sum extends BiEquationPart {
    public Sum(final EquationPart left, final EquationPart right) {
        super(left, right, Operations.SUM.getOperator());
    }

    @Override
    public Double eval(final Map<String, Double> values) {
        return left.eval(values) + right.eval(values);
    }
}