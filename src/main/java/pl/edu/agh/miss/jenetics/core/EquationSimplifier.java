package pl.edu.agh.miss.jenetics.core;

import pl.edu.agh.miss.jenetics.beans.EquationPart;

public interface EquationSimplifier {
    EquationPart simplify(EquationPart equationPart);
}