package pl.edu.agh.miss.jenetics.core;

import pl.edu.agh.miss.jenetics.beans.EquationPart;

public interface EquationValidator {
    boolean isValid(EquationPart equationPart);
}
