package pl.edu.agh.miss.jenetics.core.fitness;

import org.jenetics.Genotype;
import pl.edu.agh.miss.jenetics.beans.EquationChromosome;
import pl.edu.agh.miss.jenetics.beans.EquationGene;
import pl.edu.agh.miss.jenetics.beans.EquationPart;
import pl.edu.agh.miss.jenetics.core.EquationFitnessFunction;

import java.math.BigDecimal;
import java.util.AbstractMap.SimpleEntry;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static java.util.Collections.unmodifiableList;
import static java.util.Objects.requireNonNull;

public class SimpleEquationDispersionFitnessFunction implements EquationFitnessFunction {
    private final List<String> variables;
    private final List<List<Double>> values;
    private final List<Double> results;

    public SimpleEquationDispersionFitnessFunction(final List<String> variables,
                                                   final List<List<Double>> values,
                                                   final List<Double> results) {
        this.variables = unmodifiableList(variables);
        this.values = unmodifiableList(values);
        this.results = unmodifiableList(results);
    }

    @Override
    public Integer eval(final Genotype<EquationGene> genotype) {
        requireNonNull(genotype);
        final EquationPart equation = ((EquationChromosome) genotype.getChromosome()).getEquation();
        final List<Double> equationResults = values.stream()
                .map(values -> IntStream.range(0, variables.size())
                        .mapToObj(i -> new SimpleEntry<>(variables.get(i), values.get(i)))
                        .collect(Collectors.toMap(SimpleEntry::getKey, SimpleEntry::getValue)))
                .map(equation::eval)
                .map(Math::abs)
                .collect(Collectors.toList());
        return (int) (100 - IntStream.range(0, equationResults.size())
                .mapToObj(i -> equationResults.get(i) - (results.get(i)))
                .map(Math::abs)
                .mapToDouble(d -> d)
                .sum());
    }
}