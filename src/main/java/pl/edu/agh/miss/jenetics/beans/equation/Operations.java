package pl.edu.agh.miss.jenetics.beans.equation;

public enum Operations {
    SUM("+"), SUBSTRACT("-"), MULTIPLY("*"), DIVIDE("/"), POWER("^"),
    LOGATIHM("ln"), SINUS("sin"), COSINUS("cos"), UNKNOWN("");

    private String operator;

    Operations(String operator) {
        this.operator = operator;
    }

    public String getOperator() {
        return operator;
    }
}
