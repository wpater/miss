package pl.edu.agh.miss.jenetics.core;

import pl.edu.agh.miss.jenetics.beans.EquationPart;

import java.util.List;

public interface EquationSplitter {
    List<EquationPart> split(EquationPart equationPart);
}
