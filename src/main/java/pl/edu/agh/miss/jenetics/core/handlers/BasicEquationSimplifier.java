package pl.edu.agh.miss.jenetics.core.handlers;

import pl.edu.agh.miss.jenetics.beans.EquationPart;
import pl.edu.agh.miss.jenetics.beans.equation.BiEquationPart;
import pl.edu.agh.miss.jenetics.beans.equation.Constant;
import pl.edu.agh.miss.jenetics.beans.equation.SingleEquationPart;
import pl.edu.agh.miss.jenetics.beans.equation.Variable;
import pl.edu.agh.miss.jenetics.core.EquationSimplifier;


import static java.util.Objects.requireNonNull;

public class BasicEquationSimplifier implements EquationSimplifier {
    @Override
    public EquationPart simplify(final EquationPart equationPart) {
        requireNonNull(equationPart);
        if (equationPart.isTerminal() || equationPart instanceof SingleEquationPart) {
            return equationPart;
        } else {
            final BiEquationPart biEquationPart = (BiEquationPart) equationPart;
            if (isZeroPart(biEquationPart)) {
                switch (biEquationPart.getOperator()) {
                    case "*":
                    case "/":
                        return new Constant(0.0);
                    case "+":
                        return isZero(biEquationPart.getLeft()) ?
                                biEquationPart.getRight() :
                                biEquationPart.getLeft();
                    case "-":
                        return isZero(biEquationPart.getRight()) ?
                                biEquationPart.getLeft() : equationPart;
                    default:
                        return equationPart;
                }
            } else if (isBothConstans(biEquationPart)) {
                return new Constant(biEquationPart.eval());
            } else if (isSameVariablesSubtract(biEquationPart)) {
                return new Constant(1.0);
            } else {
                return biEquationPart
                        .withLeft(simplify(biEquationPart.getLeft()), biEquationPart.getClass())
                        .withRight(simplify(biEquationPart.getRight()), biEquationPart.getClass());
            }
        }
    }

    private boolean isSameVariablesSubtract(final BiEquationPart biEquationPart) {
        return biEquationPart.getLeft() instanceof Variable &&
                biEquationPart.getRight() instanceof Variable &&
                biEquationPart.getLeft().toString().equals(biEquationPart.getRight().toString());
    }

    private boolean isBothConstans(final BiEquationPart biEquationPart) {
        return biEquationPart.getLeft() instanceof Constant &&
                biEquationPart.getRight() instanceof Constant;
    }

    private boolean isZeroPart(final BiEquationPart biEquationPart) {
        return isZero(biEquationPart.getLeft()) || isZero(biEquationPart.getRight());
    }

    private boolean isZero(final EquationPart equationPart) {
        return equationPart instanceof Constant && equationPart.eval() == 0;
    }
}