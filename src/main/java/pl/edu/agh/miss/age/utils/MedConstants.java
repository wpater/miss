package pl.edu.agh.miss.age.utils;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public interface MedConstants {
    // Declare list of operations for equation
    List<String> operations = Stream.of("sum", "subtract", "multiply", "divide", "power", "ln", "sin", "cos")
            .collect(Collectors.toList());
}
