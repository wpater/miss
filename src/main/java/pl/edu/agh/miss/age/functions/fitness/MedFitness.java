package pl.edu.agh.miss.age.functions.fitness;

import pl.edu.agh.miss.jenetics.beans.EquationPart;
import pl.edu.agh.miss.parser.structure.MedData;

import java.util.List;

public interface MedFitness {
    Double fitnessSinus(EquationPart root, List<String> variables);
    Double fitnessSinusTwoDimensions(EquationPart root, List<String> variables);
    Double fitness(EquationPart root);
    MedData getData();
}
