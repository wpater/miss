package pl.edu.agh.miss.age.functions.recombination;

import pl.edu.agh.miss.jenetics.beans.EquationPart;
import pl.edu.agh.miss.jenetics.beans.equation.BiEquationPart;

public interface MedRecombination {

    EquationPart recombinateTree(EquationPart first, EquationPart second);
}
