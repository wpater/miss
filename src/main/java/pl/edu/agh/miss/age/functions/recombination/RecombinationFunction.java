package pl.edu.agh.miss.age.functions.recombination;

import com.google.common.math.DoubleMath;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.edu.agh.age.compute.stream.emas.reproduction.recombination.Recombination;
import pl.edu.agh.age.compute.stream.emas.solution.SimpleSolution;
import pl.edu.agh.miss.age.functions.fitness.MedFitness;
import pl.edu.agh.miss.age.functions.fitness.MedFitnessEvaluator;
import pl.edu.agh.miss.age.utils.MedConstants;
import pl.edu.agh.miss.jenetics.beans.EquationPart;

import static pl.edu.agh.miss.age.utils.MathConstants.TOLERANCE;

public class RecombinationFunction implements Recombination<SimpleSolution<EquationPart>> {

    private final MedFitness fitnessEvaluator;

    public RecombinationFunction(MedFitness fitnessEvaluator) {
        this.fitnessEvaluator = fitnessEvaluator;
    }

    @Override
    public SimpleSolution<EquationPart> recombine(
            SimpleSolution<EquationPart> firstSolution,
            SimpleSolution<EquationPart> secondSolution) {
        final EquationPart equation1 = firstSolution.unwrap();
        final EquationPart equation2 = secondSolution.unwrap();
        final double firstV = fitnessEvaluator.fitness(equation1);
        final double secondV = fitnessEvaluator.fitness(equation2);
        return DoubleMath.fuzzyCompare(Math.abs(firstV), Math.abs(secondV), TOLERANCE) < 0 ? firstSolution : secondSolution;
    }
}
