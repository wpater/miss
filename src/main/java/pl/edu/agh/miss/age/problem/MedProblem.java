package pl.edu.agh.miss.age.problem;

import pl.edu.agh.age.compute.stream.logging.DefaultLoggingService;
import pl.edu.agh.age.compute.stream.problem.ProblemDefinition;

public class MedProblem implements ProblemDefinition{

    @Override
    public String representation() {
        return "Generating functions that fill to given list of points. Trying to find the best solution for given data.";
    }
}
