package pl.edu.agh.miss.age.generators;

import pl.edu.agh.age.compute.stream.emas.EmasAgent;
import pl.edu.agh.age.compute.stream.emas.solution.SimpleSolution;
import pl.edu.agh.age.compute.stream.emas.solution.Solution;
import pl.edu.agh.miss.age.functions.fitness.MedFitness;
import pl.edu.agh.miss.age.functions.fitness.MedFitnessEvaluator;
import pl.edu.agh.miss.age.utils.MedConstants;
import pl.edu.agh.miss.jenetics.beans.EquationPart;
import pl.edu.agh.miss.jenetics.core.EquationGenerator;
import pl.edu.agh.miss.jenetics.core.EquationSimplifier;
import pl.edu.agh.miss.jenetics.core.generators.BasicEquationGenerator;
import pl.edu.agh.miss.jenetics.core.handlers.BasicEquationSimplifier;
import pl.edu.agh.miss.jenetics.core.validators.DefaultEquationValidator;

import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class MedAgentRandomGenerator {

    private static MedFitness evaluator;
    private static EquationSimplifier simplifier;

    /**
     * Create new Agent with random energy and solution
     * @param fitnessEvaluator Evaluator for fitness
     * @return randomed EmasAgent
     */
    public static EmasAgent randomAgent(MedFitnessEvaluator fitnessEvaluator) {
        ThreadLocalRandom randomGenerator = ThreadLocalRandom.current();
        MedAgentRandomGenerator.evaluator = fitnessEvaluator;
        MedAgentRandomGenerator.simplifier = new BasicEquationSimplifier();
        return EmasAgent.create(getRandomEnergy(randomGenerator), getRandomSolution(randomGenerator));
    }

    private static double getRandomEnergy(ThreadLocalRandom randomGenerator) {
        return 1 + randomGenerator.nextDouble(-0.1, 0.1);
    }

    private static Solution<?> getRandomSolution(ThreadLocalRandom randomGenerator) {
        final Random random = new Random();
        final List<String> variables = evaluator.getData().getVariables();
        final DefaultEquationValidator defaultEquationValidator = new DefaultEquationValidator();
        final EquationGenerator dummyEquationGenerator = new BasicEquationGenerator(
                defaultEquationValidator,
                variables,
                MedConstants.operations,
                random, 4, 4, 3);
        EquationPart equation = dummyEquationGenerator.generate();
        boolean flag = true;
        Double val = 0.0;
        while (flag) {
            val = evaluator.fitness(equation);
            if (!val.isNaN() && !val.isInfinite()) flag = false;
            else equation = dummyEquationGenerator.generate();
        }
        return new SimpleSolution<>(simplifier.simplify(equation), val);
    }
}
