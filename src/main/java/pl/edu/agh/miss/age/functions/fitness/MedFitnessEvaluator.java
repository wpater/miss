package pl.edu.agh.miss.age.functions.fitness;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.edu.agh.age.compute.stream.emas.solution.SimpleSolution;
import pl.edu.agh.age.compute.stream.problem.Evaluator;
import pl.edu.agh.miss.jenetics.beans.EquationPart;
import pl.edu.agh.miss.parser.Parser;
import pl.edu.agh.miss.parser.SimpleParser;
import pl.edu.agh.miss.parser.structure.MedData;

import java.util.*;

//TODO: Try to remove MedFitness interface, refactoring
public class MedFitnessEvaluator implements MedFitness, Evaluator<SimpleSolution<EquationPart>> {

    private final Logger logger = LoggerFactory.getLogger(MedFitnessEvaluator.class);
    private MedData data;
    private List<Double> varsForSinus = Arrays.asList(1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0, 11.0,
            12.0, 13.0, 14.0, 15.0, 16.0, 17.0, 18.0, 19.0, 20.0, 21.0, 22.0, 23.0, 24.0, 25.0);

    public MedFitnessEvaluator(String pathToData) {
        Parser parser = new SimpleParser(pathToData);
        data = parser.parse();
    }

    /**
     * Calculate fitness for given tree with equation
     * @param root Tree with equation
     * @return fitness
     */
    @Override
    public Double fitness(EquationPart root) {
        Double fitness = 0.0;
        List<Map<String, Double>> vars = data.getPreparedPoints();
        for (Map<String, Double> v : vars) {
            Double treeValue = root.eval(v);
            fitness += Math.pow((treeValue - v.get("result")), 2);
        }
        // Change here from debug to info to pront fitness for each generated function
        logger.debug("Fitness for function: " + root + "\nFitness is: " + fitness);
        return fitness;
    }

    /**
     * Fitness method for function x*sin(x)
     * @param root Tree with equation
     * @param variables List of variables
     * @return fitness
     */
    @Override
    public Double fitnessSinus(EquationPart root, List<String> variables) {
        Double fitness = 0.0;
        List<Map<String, Double>> vars = createVarsForSinus(variables);
        for (Map<String, Double> v : vars) {
            Double treeValue = root.eval(v);
            Double sinusValue = (Math.sin(v.get("x")) * v.get("x"));
            fitness += Math.pow((treeValue - sinusValue), 2);
        }
        logger.debug("Fitness for Sinus function - value: " + fitness + " tree: " + root);
        return fitness;
    }

    /**
     * Fitness method for function sin(x) + sin(y)
     * @param root Tree with equation
     * @param variables List of variables
     * @return fitness
     */
    @Override
    public Double fitnessSinusTwoDimensions(EquationPart root, List<String> variables) {
        Double fitness = 0.0;
        List<Map<String, Double>> vars = createVarsForSinus(variables);
        for (Map<String, Double> v : vars) {
            Double treeValue = root.eval(v);
            Double sinusValue = (Math.sin(v.get("x")) + Math.sin(v.get("y")));
            fitness += Math.pow((treeValue - sinusValue), 2);
        }
        logger.debug("Fitness for Sinus function - value: " + fitness + " tree: " + root);
        return fitness;
    }

    private List<Map<String, Double>> createVarsForSinus(List<String> variables) {
        List<Map<String, Double>> vars = new ArrayList<>();
        for (Double x : varsForSinus) {
            Map<String, Double> m = new HashMap<>();
            for (String v : variables) {
                m.put(v, x);
            }
            vars.add(m);
        }
        return vars;
    }

    /**
     * Calculate fitness for given solution
     * @param equationPartSimpleSolution Solution with tree with equation
     * @return fitness value
     */
    @Override
    public double evaluate(SimpleSolution<EquationPart> equationPartSimpleSolution) {
        return fitness(equationPartSimpleSolution.unwrap());
    }

    public MedData getData() {
        return data;
    }
}
