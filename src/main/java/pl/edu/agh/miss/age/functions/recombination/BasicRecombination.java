package pl.edu.agh.miss.age.functions.recombination;

import pl.edu.agh.age.compute.stream.emas.reproduction.recombination.Recombination;
import pl.edu.agh.age.compute.stream.emas.solution.SimpleSolution;
import pl.edu.agh.miss.age.functions.fitness.MedFitness;
import pl.edu.agh.miss.jenetics.beans.EquationPart;
import pl.edu.agh.miss.jenetics.core.EquationSimplifier;
import pl.edu.agh.miss.jenetics.core.handlers.BasicEquationRandomizer;
import pl.edu.agh.miss.jenetics.core.handlers.BasicEquationSimplifier;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class BasicRecombination implements MedRecombination, Recombination<SimpleSolution<EquationPart>> {

    private BasicEquationRandomizer equationRandomizer;
    private final MedFitness fitnessEvaluator;
    private EquationSimplifier simplifier;

    public BasicRecombination(MedFitness fitnessEvaluator) {
        equationRandomizer = new BasicEquationRandomizer();
        this.fitnessEvaluator = fitnessEvaluator;
        simplifier = new BasicEquationSimplifier();
    }

    @Override
    public EquationPart recombinateTree(EquationPart first, EquationPart second) {
        final ThreadLocalRandom randomGenerator = ThreadLocalRandom.current();
        EquationPart node1 = equationRandomizer.randomNodeFromTree(1, 10, first);
        List<EquationPart> path1 = equationRandomizer.getPath();
        EquationPart node2 = equationRandomizer.randomNodeFromTree(1, 10, second);
        return equationRandomizer.joinTrees(path1, node1, node2);
    }

    @Override
    public SimpleSolution<EquationPart> recombine(SimpleSolution<EquationPart> s1, SimpleSolution<EquationPart> s2) {
        EquationPart first = s1.unwrap();
        EquationPart second = s2.unwrap();
        boolean flag = true;
        Double val;
        EquationPart newNode = recombinateTree(first, second);
        int tries = 0;
        while (flag && tries < 5) {
            val = fitnessEvaluator.fitness(newNode);
            if (!val.isNaN() && !val.isInfinite()) flag = false;
            else newNode = recombinateTree(first, second);
            tries++;
        }
        if (tries < 5) return new SimpleSolution<>(simplifier.simplify(newNode), fitnessEvaluator.fitness(newNode));
        return s1;
    }
}
