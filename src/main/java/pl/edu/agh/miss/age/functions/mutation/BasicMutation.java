package pl.edu.agh.miss.age.functions.mutation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.edu.agh.age.compute.stream.emas.reproduction.mutation.Mutation;
import pl.edu.agh.age.compute.stream.emas.solution.SimpleSolution;
import pl.edu.agh.miss.age.functions.fitness.MedFitness;
import pl.edu.agh.miss.age.functions.fitness.MedFitnessEvaluator;
import pl.edu.agh.miss.age.utils.MedConstants;
import pl.edu.agh.miss.jenetics.beans.EquationPart;
import pl.edu.agh.miss.jenetics.beans.equation.BiEquationPart;
import pl.edu.agh.miss.jenetics.beans.equation.SingleEquationPart;
import pl.edu.agh.miss.jenetics.core.EquationGenerator;
import pl.edu.agh.miss.jenetics.core.EquationRandomizer;
import pl.edu.agh.miss.jenetics.core.EquationSimplifier;
import pl.edu.agh.miss.jenetics.core.generators.BasicEquationGenerator;
import pl.edu.agh.miss.jenetics.core.handlers.BasicEquationRandomizer;
import pl.edu.agh.miss.jenetics.core.handlers.BasicEquationSimplifier;
import pl.edu.agh.miss.jenetics.core.validators.DefaultEquationValidator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;


public class BasicMutation implements MedMutation, Mutation<SimpleSolution<EquationPart>> {

    private final Logger logger = LoggerFactory.getLogger(BasicMutation.class);

    //TODO: use parameters for mutation
    private final double mutationProbability;
    private final double mutationDispersion;
    private final MedFitness fitness;
    private EquationSimplifier simplifier;
    private BasicEquationRandomizer equationRandomizer;

    public BasicMutation(double mutationProbability, double mutationDispersion, MedFitness fitness) {
        this.mutationProbability = mutationProbability;
        this.mutationDispersion = mutationDispersion;
        this.fitness = fitness;
        simplifier = new BasicEquationSimplifier();
        equationRandomizer = new BasicEquationRandomizer();
    }

    /**
     * Mutate generated solution to the new one. This method is used by AgE framework
     * @param solution Solution with tree with equation
     * @return Solution with tree with equation
     */
    @Override
    public SimpleSolution<EquationPart> mutate(SimpleSolution<EquationPart> solution) {
        final ThreadLocalRandom randomGenerator = ThreadLocalRandom.current();
        int rand = randomGenerator.nextInt(100);
        if (rand <= mutationProbability * 100) {
            final EquationPart tree = solution.unwrap();
            EquationPart equation = mutateTree(tree);
            boolean flag = true;
            Double val = 0.0;
            while (flag) {
                val = fitness.fitness(equation);
                if (!val.isNaN() && !val.isInfinite()) flag = false;
                else equation = mutateTree(tree);
            }
            return new SimpleSolution<>(simplifier.simplify(equation), val);
        }
        return solution;
    }

    /**
     * Mutate generated tree to the new one.
     * @param root Tree with equation
     * @return Different tree with equation
     */
    @Override
    public EquationPart mutateTree(EquationPart root) {
        EquationPart tempRoot;
        final ThreadLocalRandom randomGenerator = ThreadLocalRandom.current();
        // Go to random node
        logger.debug("Whole tree is: " + root);
        tempRoot = equationRandomizer.randomNodeFromTree(1, (int) (100 * mutationDispersion), root);
        List<EquationPart> path = equationRandomizer.getPath();
        logger.debug("Node to change is: " + tempRoot);
        logger.debug("Path from root to node is: " + path.toString());
        EquationPart newPart = generateNewPart(randomGenerator, tempRoot);
        logger.debug("New node is: " + newPart);
        tempRoot = equationRandomizer.joinTrees(path, tempRoot, newPart);
        logger.debug("Tree after mutation: " + tempRoot);
        return tempRoot;
    }

    // Generate new part of tree
    // Declare variables in constructor for generator
    private EquationPart generateNewPart(ThreadLocalRandom randomGenerator, EquationPart nodeToChange){
        final DefaultEquationValidator defaultEquationValidator = new DefaultEquationValidator();
        final EquationGenerator equationGenerator = new BasicEquationGenerator(
                defaultEquationValidator,
                fitness.getData().getVariables(),
                MedConstants.operations,
                randomGenerator, 2, 4, 3);
        EquationPart newNode = equationGenerator.generate();
        // Be sure that new node is different than old node
        while (nodeToChange.toString().equals(newNode.toString())) {
            newNode = equationGenerator.generate();
        }
        return newNode;
    }
}
