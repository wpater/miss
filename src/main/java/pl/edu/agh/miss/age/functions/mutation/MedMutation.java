package pl.edu.agh.miss.age.functions.mutation;

import pl.edu.agh.miss.jenetics.beans.EquationPart;

public interface MedMutation {
    EquationPart mutateTree(EquationPart root);
}
