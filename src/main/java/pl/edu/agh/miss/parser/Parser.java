package pl.edu.agh.miss.parser;

import pl.edu.agh.miss.parser.structure.MedData;

public interface Parser {
    MedData parse();
}
