package pl.edu.agh.miss.parser.structure;

import java.util.List;
import java.util.Map;

public class MedData {

    private String equation;
    private List<Map<String, Double>> preparedPoints;
    private List<String> variables;

    public List<Map<String, Double>> getPreparedPoints() {
        return preparedPoints;
    }

    public void setPreparedPoints(List<Map<String, Double>> preparedPoints) {
        this.preparedPoints = preparedPoints;
    }

    public String getEquation() {
        return equation;
    }

    public void setEquation(String equation) {
        this.equation = equation;
    }

    public List<String> getVariables() {
        return variables;
    }

    public void setVariables(List<String> variables) {
        this.variables = variables;
    }
}
