package pl.edu.agh.miss.parser;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.edu.agh.miss.parser.structure.MedData;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class SimpleParser implements Parser {

    private final Logger logger = LoggerFactory.getLogger(SimpleParser.class);
    private final String path;
    private MedData data;

    public SimpleParser(String path) {
        this.path = path;
    }

    /**
    Structure of file:
        equation
        dimension
        number of columns of points - In{number}
        point:result in this point
    Example:
        In1 + In2 + In3
        3
        1 2 3
        0.0 0.0 0.0:0.0
        1.0 1.0 1.0:3.0
        2.0 2.0 2.0:6.0
     */
    @Override
    public MedData parse() {
        int number = 0;
        try {
            FileReader input = new FileReader(path);
            BufferedReader reader = new BufferedReader(input);
            String line;
            List<Map<String, Double>> list = new ArrayList<>();
            Set<String> vars = new HashSet<>();
            String equation = reader.readLine();
            number++;
            String[] definedVars = reader.readLine().split(" ");
            number++;
            while ( (line = reader.readLine()) != null)
            {
                Map<String, Double> map = new HashMap<>();
                String[] wholeLine = line.split(":");
                String[] point = wholeLine[0].split(" ");
                for (String v : definedVars) {
                    String var = "In" + v;
                    vars.add(var);
                    map.put(var, Double.parseDouble(point[Integer.parseInt(v) - 1]));
                }
                map.put("result", Double.parseDouble(wholeLine[1]));
                list.add(map);
                number++;
            }
            logger.info("Parser for " + path + " started.\nEquation is: " + equation + "\nVariables: " + vars);
            logger.info("DVariables: " + definedVars);
            logger.debug("Loaded list of points is: " + list);
            data = new MedData();
            data.setPreparedPoints(list);
            data.setEquation(equation);
            data.setVariables(new ArrayList<>(vars));
            return data;
        } catch (IOException e) {
            logger.error("Fail during processing " + number + " line.");
            e.printStackTrace();
        }
        return null;
    }

    public MedData getData() {
        return data;
    }
}
